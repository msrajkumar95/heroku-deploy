# heroku-deploy

Heroku Build and Deploy

# Bitbucket Pipelines Pipe: Heroku deploy

Deploy your application to Heroku

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/heroku-deploy:1.1.0
  variables:
    HEROKU_API_KEY: '<string>'
    HEROKU_APP_NAME: '<string>'
    ZIP_FILE: '<string>'
    # WAIT: '<boolean>' # Optional.
    # DEBUG: '<boolean>' # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| HEROKU_APP_NAME (*)   | Your application name |
| HEROKU_API_KEY (*)       | Heroku API key.  |
| ZIP_FILE (*)          | Name of the zip file containing your sources |
| WAIT                  | Wait until the Heroku deployment completes. Default: `false`.|
| DEBUG                 | Turn on extra debug information. Default: `false`. |


_(*) = required variable._

## Prerequisites

To start using this pipe you need to have a [Heroku account](https://dashboard.heroku.com/apps). In addition to that you have to provide
an API key. You can get this either from your [account settings page](https://dashboard.heroku.com/account) or by using the Hekoru CLI util:

```bash
$ heroku auth:token
```

The pipe will deploy your application using Heroku Platfrom API and will authenticate requests using the provided token.
Read the official [Heroku documentation](https://devcenter.heroku.com/articles/authentication) for more details.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/heroku-deploy:1.1.0
    variables:
      HEROKU_API_KEY: $HEROKU_API_KEY
      HEROKU_APP_NAME: 'My awesome shiny app'
      ZIP_FILE: 'your-app-sources.tar.gz'
```

Advanced example:

You can set the `WAIT` parameter to `true` to tell the pipe to wait for the Heroku build completion:

```yaml
script:
  - pipe: atlassian/heroku-deploy:1.1.0
    variables:
      HEROKU_API_KEY: $HEROKU_API_KEY
      HEROKU_APP_NAME: 'My awesome shiny app'
      ZIP_FILE: 'your-app-sources.tar.gz'
      WAIT: 'true' # wait for build completion and exit the pipe
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community](https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,deployment,heroku).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
