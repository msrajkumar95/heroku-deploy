colorama==0.3.9
colorlog==4.0.2
requests2==2.16.0
pyaml==18.11.0
bitbucket_pipes_toolkit==1.7.0