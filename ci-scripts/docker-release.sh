#!/usr/bin/env bash

DOCKERHUB_USERNAME='msrajkumar95'
DOCKERHUB_PASSWORD='ms8438440068'

set -ex

IMAGE=$1
VERSION=$(semversioner current-version)

echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
docker build -t ${IMAGE} -t ${IMAGE}:${VERSION} -t ${IMAGE}:latest .
docker tag ${IMAGE} ${IMAGE}:${VERSION}
docker push ${IMAGE}
